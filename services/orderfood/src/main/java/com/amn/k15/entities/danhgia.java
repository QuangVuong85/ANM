package com.amn.k15.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class danhgia {
    @Id
    private ObjectId _id;
    private Integer id;
    private Integer mamon;
    private String noidung;
    private Integer index;
    private Integer ngaytao;

    public danhgia(ObjectId _id, Integer id, Integer mamon, String noidung, Integer index, Integer ngaytao) {
        this._id = _id;
        this.id = id;
        this.mamon = mamon;
        this.noidung = noidung;
        this.index = index;
        this.ngaytao = ngaytao;
    }

    public danhgia() {
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMamon() {
        return mamon;
    }

    public void setMamon(Integer mamon) {
        this.mamon = mamon;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getNgaytao() {
        return ngaytao;
    }

    public void setNgaytao(Integer ngaytao) {
        this.ngaytao = ngaytao;
    }
}
