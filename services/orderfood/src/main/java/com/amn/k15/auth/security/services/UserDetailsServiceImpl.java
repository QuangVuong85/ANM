package com.amn.k15.auth.security.services;

import java.util.List;
import java.util.Optional;

import com.amn.k15.auth.models.User;
import com.amn.k15.auth.repository.UserRepository;
import com.amn.k15.auth.security.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    //@Transactional
    public UserDetails loadUserByUsername(String tendn) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(tendn);
                //.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + tendn));

        System.out.printf(">>>> %s", user.getUsername());

        return UserDetailsImpl.build(user);
    }

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations(WebSecurityConfig.CLASSPATH_RESOURCE_LOCATIONS);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public List<User> listAll() {
        return userRepository.findAll();
    }

    public User getbyname(String username) {
        return userRepository.findByUsername(username);
    }
}
