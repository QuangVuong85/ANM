package com.amn.k15.service;

import com.amn.k15.entities.danhgia;
import com.amn.k15.utils.Response.DanhgiaRes;

import java.util.List;

public interface danhgiaService {
    void save(danhgia danhgia);
    List<danhgia> getall(Integer mamon);
    List<danhgia> findAll();
}
