package com.amn.k15.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.math.BigInteger;

public class datdoan {
    @Id
    private ObjectId _id;
    private Integer madon;
    private Integer mamon;
    private Integer soluong;
    private Long thanhtien;

    public datdoan(ObjectId _id, Integer madon, Integer mamon, Integer soluong, Long thanhtien) {
        this._id = _id;
        this.madon = madon;
        this.mamon = mamon;
        this.soluong = soluong;
        this.thanhtien = thanhtien;
    }

    public datdoan() {
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Integer getMadon() {
        return madon;
    }

    public void setMadon(Integer madon) {
        this.madon = madon;
    }

    public Integer getMamon() {
        return mamon;
    }

    public void setMamon(Integer mamon) {
        this.mamon = mamon;
    }

    public Integer getSoluong() {
        return soluong;
    }

    public void setSoluong(Integer soluong) {
        this.soluong = soluong;
    }

    public Long getThanhtien() {
        return thanhtien;
    }

    public void setThanhtien(Long thanhtien) {
        this.thanhtien = thanhtien;
    }
}
