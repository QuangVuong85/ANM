package com.amn.k15.service;

import com.amn.k15.entities.donhang;
import org.bson.types.ObjectId;

import java.util.List;

public interface donhangService {
    List<donhang> findAll();

    donhang findById(Integer id);

    donhang save(donhang hd);

    void deleteById(ObjectId id);

    donhang update(donhang hd);

    boolean exists(Integer id);

    donhang findBy_id(ObjectId id);

    void setTongtien(Long t, Integer madon);
}
