package com.amn.k15.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Document(collection = "trangthaidonhang")
public class trangthaidonhang {
    @Id
    private ObjectId _id;

    private Integer matrangthai;
    private Integer madon;
    private String noidung;

    @Override
    public String toString() {
        return "trangthaidonhang{" +
                "_id=" + _id +
                ", matrangthai=" + matrangthai +
                ", madon=" + madon +
                ", noidung='" + noidung + '\'' +
                ", ngaytao=" + ngaytao +
                '}';
    }

    private Integer ngaytao;

    public trangthaidonhang(ObjectId _id, Integer matrangthai, Integer madon, String noidung, Integer ngaytao) {
        this._id = _id;
        this.matrangthai = matrangthai;
        this.madon = madon;
        this.noidung = noidung;
        this.ngaytao = ngaytao;
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public trangthaidonhang() {
    }

    public Integer getMatrangthai() {
        return matrangthai;
    }

    public void setMatrangthai(Integer matrangthai) {
        this.matrangthai = matrangthai;
    }

    public Integer getMadon() {
        return madon;
    }

    public void setMadon(Integer madon) {
        this.madon = madon;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }

    public Integer getNgaytao() {
        return ngaytao;
    }

    public void setNgaytao(Integer ngaytao) {
        this.ngaytao = ngaytao;
    }
}
