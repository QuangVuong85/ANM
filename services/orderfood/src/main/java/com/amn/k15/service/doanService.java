package com.amn.k15.service;

import com.amn.k15.entities.doan;
import com.amn.k15.utils.Response.DoanRes;

import java.util.List;

public interface doanService {
    List<doan> listAll();

    void save(doan food);

    doan get(Integer id);

    void delete(Integer id);

    boolean exists(Integer id);

    List<doan> lists(int num);

    List<doan> listsRand(int num);

    List<doan> listsSearch(String name);

    List<doan> listsSearchFillter(String name, int idtype);

    List<doan> listsFillerType(int num, int idtype);

    List<doan> listsFillerTypeAll(int idtype);

    List<DoanRes> listView();

    Long gia(Integer mamon);
}
