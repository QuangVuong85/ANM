package com.amn.k15.controller;

import com.amn.k15.entities.datdoan;
import com.amn.k15.entities.donhang;
import com.amn.k15.entities.trangthaidonhang;
import com.amn.k15.service.datdoanService;
import com.amn.k15.service.doanService;
import com.amn.k15.service.donhangService;
import com.amn.k15.service.trangthaidonhangService;
import com.amn.k15.utils.Request.DatdoanReq;
import com.amn.k15.utils.Request.DonhangReq;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@RestController
public class donhangController {
    @Autowired
    private donhangService dhService;

    @Autowired
    private trangthaidonhangService serviceTt;

    @Autowired
    private doanService serviceDoa;

    @Autowired
    private datdoanService serviceDat;

    @PutMapping("/orders/{id}")
    public ResponseEntity<?> update(@RequestBody donhang donhang, @PathVariable Integer id) {
        boolean check = dhService.exists(id);
        if (check) {
            donhang dh = dhService.findById(id);
            donhang.set_id(dh.get_id());
            dhService.save(donhang);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/orders")
    public void add(@RequestBody DonhangReq res, @RequestHeader Map<String, String> headers) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Long time = timestamp.getTime();
        donhang donhang = new donhang(
                ObjectId.get(),
                4,
                1111,
                res.getTenkhach(),
                res.getSdt(),
                0L,
                res.getGhichu(),
                res.getDiachinhan(),
                0L,
                time);

        donhang donhangIns = dhService.save(donhang);

        Integer ma = donhangIns.getMadon();

        Long tong = 0L;
        serviceTt.save(new trangthaidonhang(
                ObjectId.get(),
                8,
                ma,
                "",
                time.intValue()));
        for (DatdoanReq item : res.getDoan()) {
            Long temp = serviceDoa.gia(item.getMamon()) * item.getsoluong();
            datdoan datdoan = new datdoan(
                    ObjectId.get(),
                    ma, item.getMamon(),
                    item.getsoluong(),
                    temp
            );
            tong += temp;
            serviceDat.save(datdoan);
        }
        dhService.setTongtien(tong, ma);
    }

    @GetMapping("/orders")
    public List<donhang> getAllDonhang() {
        return dhService.findAll();
    }

    @GetMapping("/orders/{id}")
    public donhang get(@PathVariable Integer id) {
//        return dhService.findBy_id(id);
        return dhService.findById(id);
    }

    /*@PostMapping("/orders")
    public donhang createDonhang(@Valid @RequestBody donhang dh) {
        dh.set_id(ObjectId.get());
        dhService.save(dh);
        return dh;
    }*/
}
