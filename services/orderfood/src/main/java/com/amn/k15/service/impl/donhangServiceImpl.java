package com.amn.k15.service.impl;

import com.amn.k15.entities.donhang;
import com.amn.k15.repository.donhangRepository;
import com.amn.k15.service.donhangService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class donhangServiceImpl implements donhangService {
    @Autowired
    private donhangRepository dhRepository;

    @Override
    public List<donhang> findAll() {
        return dhRepository.findAll();
    }

    @Override
    public donhang findById(Integer id) {
        return dhRepository.findByMadon(id);
    }

    @Override
    public donhang save(donhang dh) {
        dhRepository.save(dh);
        return dh;
    }

    @Override
    public void deleteById(ObjectId id) {
        dhRepository.deleteById(id);
    }

    @Override
    public donhang update(donhang dh) {
        dhRepository.save(dh);
        return dh;
    }

    @Override
    public boolean exists(Integer id) {
        return dhRepository.existsByMadon(id);
    }

    @Override
    public donhang findBy_id(ObjectId id) {
        return dhRepository.findBy_id(id);
    }

    @Override
    public void setTongtien(Long t, Integer madon) {

    }
}
