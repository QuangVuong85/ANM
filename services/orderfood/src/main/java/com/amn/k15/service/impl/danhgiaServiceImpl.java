package com.amn.k15.service.impl;

import com.amn.k15.entities.danhgia;
import com.amn.k15.repository.danhgiaRepository;
import com.amn.k15.service.danhgiaService;
import com.amn.k15.utils.Response.DanhgiaRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class danhgiaServiceImpl implements danhgiaService {
    @Autowired
    private danhgiaRepository repo;

    @Override
    public void save(danhgia danhgia) {
        repo.save(danhgia);
    }

    @Override
    public List<danhgia> getall(Integer mamon) {
        return repo.findAllByMamon(mamon);
    }

    @Override
    public List<danhgia> findAll() {
        return repo.findAll();
    }
}
