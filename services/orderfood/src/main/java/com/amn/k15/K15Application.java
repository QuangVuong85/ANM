package com.amn.k15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K15Application {
    public static void main(String[] args) {
        SpringApplication.run(K15Application.class, args);
    }
}
