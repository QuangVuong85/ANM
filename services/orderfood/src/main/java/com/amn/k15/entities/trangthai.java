package com.amn.k15.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Document(collection = "trangthai")
public class trangthai {
    @Id
    private ObjectId _id;
    private BigInteger matrangthai;
    private String tentrangthai;

    public trangthai(ObjectId _id, BigInteger matrangthai, String tentrangthai) {
        this._id = _id;
        this.matrangthai = matrangthai;
        this.tentrangthai = tentrangthai;
    }

    public trangthai() {
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public BigInteger getMatrangthai() {
        return matrangthai;
    }

    public void setMatrangthai(BigInteger matrangthai) {
        this.matrangthai = matrangthai;
    }

    public String getTentrangthai() {
        return tentrangthai;
    }

    public void setTentrangthai(String tentrangthai) {
        this.tentrangthai = tentrangthai;
    }
}
