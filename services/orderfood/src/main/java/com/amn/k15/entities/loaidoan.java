package com.amn.k15.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "loaidoan")
public class loaidoan {
    @Id
    private ObjectId _id;
    private Integer maloai;
    private String tenloai;

    public loaidoan() {
    }

    public loaidoan(ObjectId _id, Integer maloai, String tenloai) {
        this._id = _id;
        this.maloai = maloai;
        this.tenloai = tenloai;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Integer getMaloai() {
        return maloai;
    }

    public void setMaloai(Integer maloai) {
        this.maloai = maloai;
    }

    public String getTenloai() {
        return tenloai;
    }

    public void setTenloai(String tenloai) {
        this.tenloai = tenloai;
    }
}
