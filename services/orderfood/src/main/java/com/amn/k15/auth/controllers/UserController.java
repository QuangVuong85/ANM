package com.amn.k15.auth.controllers;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.websocket.server.PathParam;

import com.amn.k15.auth.repository.RoleRepository;
import com.mongodb.client.result.UpdateResult;
import jdk.jfr.internal.Repository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import com.amn.k15.auth.models.User;
import com.amn.k15.auth.models.ERole;
import com.amn.k15.auth.models.Role;
import com.amn.k15.auth.payload.request.UpdateUserRequest;
import com.amn.k15.auth.payload.response.MessageResponse;
import org.springframework.http.HttpStatus;
import com.amn.k15.auth.security.services.UserDetailsServiceImpl;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import com.amn.k15.auth.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.access.prepost.PreAuthorize;
import com.amn.k15.auth.repository.RoleRepository;

@RestController
public class UserController {
    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    private UserDetailsServiceImpl service;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleRepository roleRepository;

//    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/users")
    public List<User> listuser() {
        return service.listAll();
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<?> update(@RequestBody UpdateUserRequest userupdate, @PathVariable Integer id) {
        if (userRepository.existsByUsername(userupdate.getUsername())) {
            Set<String> strRoles = userupdate.getRole();
            Set<Role> roles = new HashSet<>();
            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "admin":
                            Role adminRole = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: CAN NOT UPGARADE TO ADMIN_ROLE --> Down to UserRole!!!"));
                            roles.add(adminRole);
                            break;
                        case "mod":
                            Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(modRole);

                            break;
                        default:
                            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(userRole);
                    }
                });
            }
            System.out.println(strRoles.iterator().next());
            if (strRoles.iterator().next().equals("user")) {
                System.out.println("hihi");
            }
            updateUser(id, userupdate.getUsername(), encoder.encode(userupdate.getPassword()), userupdate.getEmail());
            // userRepository.updateRoleUser(id, role_id);
            return new ResponseEntity<UpdateUserRequest>(userupdate, HttpStatus.OK);
        } else {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is not found!!!"));
        }
    }

    public void updateUser(Integer id, String username, String password, String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Update update = new Update();
        update.set("username", username);
        update.set("password", password);
        update.set("email", email);

        UpdateResult result = this.mongoTemplate.updateFirst(query, update, User.class);
        System.out.println(result.getModifiedCount());
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<?> deleterole(@RequestBody User user, @PathVariable Integer id) {
        if (userRepository.existsByUsername(user.getUsername())) {
            encoder.encode(user.getPassword());
            service.save(user);
            return new ResponseEntity<>("Success: Delete Role!!!", HttpStatus.OK);
        } else {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is not found!!!"));
        }
    }
}
