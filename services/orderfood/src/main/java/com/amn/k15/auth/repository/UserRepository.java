package com.amn.k15.auth.repository;

import java.util.Optional;

import com.amn.k15.auth.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, ObjectId> {
    User findByUsername(String tendn);

    Boolean existsByUsername(String tendn);

    Boolean existsByEmail(String email);

    User findBy_id(ObjectId id);

	/*@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE users set username = ?2, password = ?3, email= ?4 where id= ?1", nativeQuery = true)
	void updateUser(Integer id, String username,String password,String email);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE user_roles set role_id = ?2 where user_id = ?1", nativeQuery = true)
	void updateRoleUser(Integer user_id, Integer role_id);*/
}
