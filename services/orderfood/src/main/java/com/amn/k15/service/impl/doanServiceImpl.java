package com.amn.k15.service.impl;

import com.amn.k15.auth.models.User;
import com.amn.k15.entities.doan;
import com.amn.k15.repository.doanRepository;
import com.amn.k15.service.doanService;
import com.amn.k15.utils.Response.DoanRes;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class doanServiceImpl implements doanService {
    @Autowired
    private doanRepository repo;
    @Autowired
    MongoTemplate mongoTemplate;

    public List<doan> listAll() {
        return repo.findAll();
    }

    public void save(doan food) {
        repo.save(food);
    }

    public doan get(Integer id) {
        return repo.findByMamon(id);
    }

    public void delete(Integer id) {
        repo.deleteByMamon(id);
    }

    public boolean exists(Integer id) {
        return repo.existsByMamon(id);
    }

    public List<doan> lists(int num) {
        final PageRequest pageableRequest = PageRequest.of(0, num);
        Query query = new Query();
        query.with(pageableRequest);

        return this.mongoTemplate.find(query, doan.class);
    }

    public List<doan> listsRand(int num) {
        final PageRequest pageableRequest = PageRequest.of(0, num);
        Query query = new Query();
        query.with(pageableRequest);

        return this.mongoTemplate.find(query, doan.class);
    }

    public List<doan> listsSearch(String name) {
        System.out.println(name);
        return repo.findAllByTenmon(name);
    }

    public List<doan> listsSearchFillter(String name, int idtype) {
        System.out.println("name : " + name + " idtype : " + idtype);
        return repo.findAllByTenmon(name);
    }

    public List<doan> listsFillerType(int num, int idtype) {
        final PageRequest pageableRequest = PageRequest.of(0, num);
        Query query = new Query();
        query.with(pageableRequest);

        return this.mongoTemplate.find(query, doan.class);
    }

    public List<doan> listsFillerTypeAll(int idtype) {
        return repo.findAllByMaloai(idtype);
    }

    public List<DoanRes> listView() {
        List<DoanRes> ls = new ArrayList<>();
        return ls;
    }

    public Long gia(Integer mamon) {
        return repo.findByMamon(mamon).getGia();
    }
}
