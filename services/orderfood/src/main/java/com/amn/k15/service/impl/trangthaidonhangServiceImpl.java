package com.amn.k15.service.impl;

import com.amn.k15.entities.trangthaidonhang;
import com.amn.k15.repository.trangthaidonhangRepository;
import com.amn.k15.service.trangthaidonhangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class trangthaidonhangServiceImpl implements trangthaidonhangService {
    @Autowired
    private trangthaidonhangRepository repo;

    @Override
    public void save(trangthaidonhang ttdh) {
        repo.save(ttdh);
    }
}
