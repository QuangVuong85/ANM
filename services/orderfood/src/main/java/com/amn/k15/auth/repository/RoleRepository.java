package com.amn.k15.auth.repository;

import java.util.Optional;

import com.amn.k15.auth.models.ERole;
import com.amn.k15.auth.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}
