package com.amn.k15.controller;

import com.amn.k15.entities.loaidoan;
import com.amn.k15.service.loaidoanService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@RestController
public class loaidoanController {
    @Autowired
    private loaidoanService service;

    @PostMapping("/types")
    public void add(@RequestBody loaidoan loaidoan) {
        loaidoan.set_id(ObjectId.get());
        service.save(loaidoan);
    }

    @GetMapping("/types")
    public List<loaidoan> list() {
        return service.listAll();
    }

    @GetMapping("/types/{maloai}")
    public ResponseEntity<loaidoan> get(@PathVariable Integer maloai) {
        boolean check = service.exists(maloai);
        if (check) {
            loaidoan lda = service.findBymaloai(maloai);
            return new ResponseEntity<loaidoan>(lda, HttpStatus.OK);
        } else {
            return new ResponseEntity<loaidoan>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/types/{maloai}")
    public ResponseEntity<?> update(@RequestBody loaidoan lda, @PathVariable Integer maloai) {
        boolean check = service.exists(maloai);
        if (check) {
            loaidoan ld = service.findBymaloai(maloai);
            lda.set_id(ld.get_id());
            service.save(lda);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/types/{maloai}")
    public void delete(@PathVariable Integer maloai) {
        service.delete(maloai);
    }
}
