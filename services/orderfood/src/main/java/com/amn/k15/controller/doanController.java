package com.amn.k15.controller;

import com.amn.k15.entities.doan;
import com.amn.k15.service.doanService;
import com.amn.k15.service.loaidoanService;
import com.amn.k15.utils.Response.DoanRes;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class doanController {
    @Autowired
    private doanService service;

    @Autowired
    private loaidoanService tService;

    // create
    @PostMapping("/foods")
    //@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public void add(@RequestBody doan food) {
        food.set_id(ObjectId.get());
        service.save(food);
    }

    // get all
    @GetMapping("/foods")
    public List<doan> list(@RequestParam Optional<Integer> num, @RequestParam Optional<Integer> idtype){
        if (!num.isPresent()) {
            if(!idtype.isPresent()){
                return service.listAll();
            }else{
                boolean check = tService.exists(idtype.get());
                if (check){
                    return service.listsFillerTypeAll(idtype.get());
                }else{
                    return service.listAll();
                }
            }
        }else{
            if(!idtype.isPresent()){
                return service.lists(num.get());
            }else{
                boolean check = tService.exists(idtype.get());
                if (check){
                    return service.listsFillerType(num.get(), idtype.get());
                }else{
                    return service.lists(num.get());
                }
            }
        }
    }

    // get list view
    @GetMapping("/foods/view")
    // @PreAuthorize("hasRole('ADMIN')")
    public List<DoanRes> listView(){
        return service.listView();
    }

    // get by id
    @GetMapping("/foods/{id}")
    public ResponseEntity<doan> get(@PathVariable Integer id){
        boolean check = service.exists(id);
        if (check){
            doan food = service.get(id);
            return new ResponseEntity<doan>(food, HttpStatus.OK);
        }else{
            throw new DoanException("food not found!!!");
        }
    }

    // ex
    @ExceptionHandler(DoanException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Food not found")
    public void handlerNilException(DoanException e){
        System.out.println(e.getMessage());
    }

    // edit by id
    @PutMapping("/foods/{id}")
    //@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public ResponseEntity<?> update(@RequestBody doan food, @PathVariable Integer id) {
        boolean check = service.exists(id);
        if (check){
            doan da = service.get(id);
            food.set_id(da.get_id());
            service.save(food);
            return new ResponseEntity<doan>(food, HttpStatus.OK);
        }else{
            throw new DoanException("food not found!!!");
        }
    }

    // del by id
    @DeleteMapping("/foods/{id}")
//    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }

    // get list rand
    @GetMapping("/foods/rand")
    public List<doan> rand(@RequestParam(required = true) Optional<Integer> num){
        return service.listsRand(num.get());
    }

    @GetMapping("/foods/search")
    public List<doan> search(@RequestParam(required = true) Optional<String> name, @RequestParam Optional<Integer> idtype){
        if (!idtype.isPresent()){
            System.out.println("hihi");
            return service.listsSearch(name.get());
        }else{
            return service.listsSearchFillter(name.get(), idtype.get());
        }
    }
}

