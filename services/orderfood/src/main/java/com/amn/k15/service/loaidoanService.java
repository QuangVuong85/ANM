package com.amn.k15.service;

import com.amn.k15.entities.loaidoan;

import java.math.BigInteger;
import java.util.List;

public interface loaidoanService {
    List<loaidoan> listAll();

    void save(loaidoan loaidoan);

    loaidoan findBymaloai(Integer id);

    void delete(Integer id);

    boolean exists(Integer id);
}
