package com.amn.k15.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;
import java.util.BitSet;
import java.util.Date;

@Document(collection = "users")
public class users {
    @Id
    private ObjectId _id;
    private BigInteger id;
    private String username;
    private String password;
    private String email;
    private String gt;
    private String sdt;
    private Date ngaytao;
    private Boolean trangthai;

    public users(ObjectId _id, BigInteger id, String username, String password, String email, String gt, String sdt, Date ngaytao, boolean trangthai) {
        this._id = _id;
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.gt = gt;
        this.sdt = sdt;
        this.ngaytao = ngaytao;
        this.trangthai = trangthai;
    }

    public users() {
    }

    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGt() {
        return gt;
    }

    public void setGt(String gt) {
        this.gt = gt;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public Date getNgaytao() {
        return ngaytao;
    }

    public void setNgaytao(Date ngaytao) {
        this.ngaytao = ngaytao;
    }

    public Boolean getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(Boolean trangthai) {
        this.trangthai = trangthai;
    }
}
