package com.amn.k15.service.impl;

import com.amn.k15.entities.loaidoan;
import com.amn.k15.repository.loaidoanRepository;
import com.amn.k15.service.loaidoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class loaidoanServiceImpl implements loaidoanService {
    @Autowired
    private loaidoanRepository repo;

    public List<loaidoan> listAll() {
        return repo.findAll();
    }

    public void save(loaidoan loaidoan) {
        repo.save(loaidoan);
    }

    public loaidoan findBymaloai(Integer id) {
        return repo.findByMaloai(id);
    }

    public void delete(Integer id) {
        repo.deleteByMaloai(id);
    }

    public boolean exists(Integer id) {
        return repo.existsByMaloai(id);
    }
}
