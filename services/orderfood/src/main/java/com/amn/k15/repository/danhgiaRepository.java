package com.amn.k15.repository;

import com.amn.k15.entities.danhgia;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface danhgiaRepository extends MongoRepository<danhgia, ObjectId> {
    List<danhgia> findAllByMamon(Integer mamon);
}
