package com.amn.k15.repository;

import com.amn.k15.entities.donhang;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface donhangRepository extends MongoRepository<donhang, ObjectId> {
    donhang findBy_id(ObjectId _id);
    donhang findByMadon(Integer id);
    boolean existsByMadon(Integer id);
}
