package com.amn.k15.service.impl;

import com.amn.k15.entities.datdoan;
import com.amn.k15.repository.datdoanRepository;
import com.amn.k15.service.datdoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class datdoanServiceImpl implements datdoanService {
    @Autowired
    private datdoanRepository repo;

    @Override
    public void save(datdoan datdoan) {
        repo.save(datdoan);
    }
}
