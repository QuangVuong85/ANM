package com.amn.k15.repository;

import com.amn.k15.entities.loaidoan;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;

public interface loaidoanRepository extends MongoRepository<loaidoan, ObjectId> {
    loaidoan findByMaloai(Integer id);
    void deleteByMaloai(Integer id);
    boolean existsByMaloai(Integer id);
}
