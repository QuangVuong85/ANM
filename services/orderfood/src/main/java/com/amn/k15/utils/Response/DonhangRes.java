package com.amn.k15.utils.Response;

import java.math.BigInteger;

public interface DonhangRes {
    Integer getmadon();

    Integer getmakhach();

    BigInteger getthoigiandat();

    BigInteger getthoigiannhan();

    Integer getsoluong();

    String gettenmon();

    Long getgia();

    String getanh();
}
