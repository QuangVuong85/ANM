package com.amn.k15.repository;

import com.amn.k15.entities.trangthaidonhang;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface trangthaidonhangRepository extends MongoRepository<trangthaidonhang, ObjectId> {
}
