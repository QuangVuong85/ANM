package com.amn.k15.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "doan")
public class doan {
    @Id
    private ObjectId _id;
    private Integer mamon;
    private String tenmon;
    private Long gia;
    private String anh;
    private String mota;
    private String dvt;
    private Integer maloai;
    private String trangthai;

    public doan(ObjectId _id, Integer mamon, String tenmon, Long gia, String anh, String mota, String dvt, Integer maloai, String trangthai) {
        this._id = _id;
        this.mamon = mamon;
        this.tenmon = tenmon;
        this.gia = gia;
        this.anh = anh;
        this.mota = mota;
        this.dvt = dvt;
        this.maloai = maloai;
        this.trangthai = trangthai;
    }

    public doan() {
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Integer getMamon() {
        return mamon;
    }

    public void setMamon(Integer mamon) {
        this.mamon = mamon;
    }

    public String getTenmon() {
        return tenmon;
    }

    public void setTenmon(String tenmon) {
        this.tenmon = tenmon;
    }

    public Long getGia() {
        return gia;
    }

    public void setGia(Long gia) {
        this.gia = gia;
    }

    public String getAnh() {
        return anh;
    }

    public void setAnh(String anh) {
        this.anh = anh;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getDvt() {
        return dvt;
    }

    public void setDvt(String dvt) {
        this.dvt = dvt;
    }

    public Integer getMaloai() {
        return maloai;
    }

    public void setMaloai(Integer maloai) {
        this.maloai = maloai;
    }

    public String getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(String trangthai) {
        this.trangthai = trangthai;
    }
}
