package com.amn.k15.repository;

import com.amn.k15.entities.datdoan;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface datdoanRepository extends MongoRepository<datdoan, ObjectId> {
}
