package com.amn.k15.repository;

import com.amn.k15.entities.doan;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface doanRepository extends MongoRepository<doan, ObjectId> {
    doan findByMamon(Integer mamon);
    List<doan> findAllByTenmon(String tenmon);
    List<doan> findAllByMaloai(Integer maloai);
    void deleteByMamon(Integer mamon);
    boolean existsByMamon(Integer mamon);
}
