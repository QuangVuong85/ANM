package com.amn.k15.auth.models;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Document(collection = "users")
public class User {
//    public Integer getId() {
//        return id;
//    }

//    public void setId(Integer id) {
//        this.id = id;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    @Id
    private ObjectId _id;

//    private Integer id;

    @NotBlank
    private String username;

    @NotBlank
    @Size(max = 255)
    private String password;

    @NotBlank
    @Size(max = 250)
    @Email
    private String email;

    private String gt;

    private String sdt;

    private boolean trangthai;

    public String getGt() {
        return gt;
    }

    public void setGt(String gt) {
        this.gt = gt;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public boolean isTrangthai() {
        return trangthai;
    }

    public void setTrangthai(boolean trangthai) {
        this.trangthai = trangthai;
    }

    @Override
    public String toString() {
        return "User{" +
                "_id=" + _id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", gt='" + gt + '\'' +
                ", sdt='" + sdt + '\'' +
                ", trangthai=" + trangthai +
                ", roles=" + roles +
                '}';
    }

    public User(ObjectId _id, @NotBlank String username, @NotBlank @Size(max = 255) String password, @NotBlank @Size(max = 250) @Email String email, String gt, String sdt, boolean trangthai, Set<Role> roles) {
        this._id = _id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.gt = gt;
        this.sdt = sdt;
        this.trangthai = trangthai;
        this.roles = roles;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    private Set<Role> roles = new HashSet<>();

    public User(String username, String matkhau, String email) {
        this.username = username;
        this.password = matkhau;
        this.email = email;
    }

    public User() {
    }

}
