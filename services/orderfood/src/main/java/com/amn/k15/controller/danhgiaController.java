package com.amn.k15.controller;

import com.amn.k15.entities.danhgia;
import com.amn.k15.service.danhgiaService;
import com.amn.k15.utils.Request.DanhgiaReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
public class danhgiaController {
    @Autowired
    private danhgiaService service;

    @PostMapping("/comments")
    public void add(@RequestBody DanhgiaReq res) {
        danhgia danhgia = new danhgia();
        danhgia.setId(res.getId());
        danhgia.setMamon(res.getMamon());
        danhgia.setNoidung(res.getNoidung());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        danhgia.setNgaytao((int) timestamp.getTime());
        service.save(danhgia);
    }

    @GetMapping("/comments")
    public List<danhgia> lists(@RequestParam(required = true) Integer mamon){
        System.out.println(mamon);
        return service.getall(mamon);
    }

    @GetMapping("/comment")
    public List<danhgia> findAll(){
        return service.findAll();
    }
}
