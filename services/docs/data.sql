-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: orderfoodjava
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `danhgia`
--

DROP TABLE IF EXISTS `danhgia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `danhgia` (
  `id` int(11) NOT NULL,
  `mamon` int(11) NOT NULL,
  `noidung` varchar(100) CHARACTER SET latin1 NOT NULL,
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `ngaytao` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`index`),
  KEY `danhgia_FK` (`mamon`),
  KEY `danhgia_FK_1` (`id`),
  KEY `danhgia_ngaytao_IDX` (`ngaytao`) USING BTREE,
  CONSTRAINT `danhgia_FK` FOREIGN KEY (`mamon`) REFERENCES `doan` (`mamon`),
  CONSTRAINT `danhgia_FK_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `danhgia`
--

LOCK TABLES `danhgia` WRITE;
/*!40000 ALTER TABLE `danhgia` DISABLE KEYS */;
INSERT INTO `danhgia` VALUES (1,1,'tam duoc',1,1607131935789),(1,1,'mon ngon',2,1607131940789),(1,1,'hay day',3,1607131946257),(1,1,'xin dia chi',4,1607131954272),(1,1,'nen an thu',5,1607131981628),(1,1,'nen an thu',6,1607306681658),(2,1,'?n ngon, se? quay la?i',7,1607400613333);
/*!40000 ALTER TABLE `danhgia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datdoan`
--

DROP TABLE IF EXISTS `datdoan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datdoan` (
  `madon` int(11) NOT NULL,
  `mamon` int(11) NOT NULL,
  `soluong` int(11) DEFAULT NULL,
  `thanhtien` decimal(18,0) DEFAULT NULL,
  PRIMARY KEY (`madon`,`mamon`),
  KEY `datdoan_FK` (`mamon`),
  CONSTRAINT `datdoan_FK` FOREIGN KEY (`mamon`) REFERENCES `doan` (`mamon`),
  CONSTRAINT `datdoan_FK_1` FOREIGN KEY (`madon`) REFERENCES `donhang` (`madon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datdoan`
--

LOCK TABLES `datdoan` WRITE;
/*!40000 ALTER TABLE `datdoan` DISABLE KEYS */;
INSERT INTO `datdoan` VALUES (1,1,2,90000),(2,5,3,405000),(3,1,1,45000),(3,2,1,25000),(3,3,3,105000);
/*!40000 ALTER TABLE `datdoan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doan`
--

DROP TABLE IF EXISTS `doan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doan` (
  `mamon` int(11) NOT NULL AUTO_INCREMENT,
  `tenmon` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `gia` decimal(18,0) DEFAULT NULL,
  `anh` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `mota` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `dvt` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `maloai` int(11) DEFAULT NULL,
  `trangthai` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`mamon`),
  KEY `maloai` (`maloai`),
  CONSTRAINT `doan_ibfk_1` FOREIGN KEY (`maloai`) REFERENCES `loaidoan` (`maloai`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doan`
--

LOCK TABLES `doan` WRITE;
/*!40000 ALTER TABLE `doan` DISABLE KEYS */;
INSERT INTO `doan` VALUES (1,'Hokkaido Milk Crepes',45000,'assets/banhkem/Hokkaido%20Milk%20Crepes%20.png','','Chiếc',3,'còn hàng'),(2,'Bánh Dày Rán Ru?c',25000,'assets/doan/Banh_Day_Ran_Ruoc.png','','Suất',1,'còn hàng'),(3,'Bún Đậu Mắm Tôm',35000,'assets/doan/Bun_Dau_Mam_Tom.png','','Suất',1,'còn hàng'),(4,'Bún Đậu Tá Lả',35000,'assets/doan/Bun_dau_ta_la.png','','Suất',1,'còn hàng'),(5,'Chả Cốm',135000,'assets/doan/Cha_Com.png','','Suất',1,'còn hàng'),(6,'Chả Mực Hạ Long',145000,'assets/doan/Cha_muc_Ha_Long.png','','Suất',1,'còn hàng'),(7,'Chân Gà Chiên Nước Mắm',75000,'assets/doan/Chan_ga_chien_nuoc_mam.png','','Suất',1,'còn hàng'),(8,'Chân gà Muối Chiên',85000,'assets/doan/Chan_ga_muoi_chien.png','','Suất',1,'còn hàng'),(9,'Chân Gà Muối Sả Ớt',70000,'assets/doan/Chan_ga_muoi_sa_ot.png','','Suất',1,'còn hàng'),(10,'Chicken Steak',75000,'assets/doan/Chicken_Steak.png','','Suất',1,'còn hàng'),(11,'Cơm Rang Dưa Bò',75000,'assets/doan/Com_Rang_Dua_Bo.png','','Suất',1,'còn hàng'),(12,'Đậu Hũ Hoàng Kim',75000,'assets/doan/dau_hu_hoang_kim.png','','Suất',1,'còn hàng'),(13,'Giò Tai Rán',75000,'assets/doan/Gio_tai_ran.png','','Suất',1,'còn hàng'),(14,'Igai Mentaiko Yaki',75000,'assets/doan/Igai_Mentaiko_Yaki.png','','Suất',1,'còn hàng'),(15,'Lòng Gà Trứng Non Cháy Tỏi',75000,'assets/doan/Long_ga_trung_non_chay_toi.png','','Suất',1,'còn hàng'),(16,'Nem Chua Rán',75000,'assets/doan/Nem_Chua_Ran.png','','Suất',1,'còn hàng'),(17,'Nem Tai Thính',75000,'assets/doan/Nem_tai_Thinh.png','','Suất',1,'còn hàng'),(18,'Ngọc Kê lòng Gà Trứng Non Cháy Tỏi',75000,'assets/doan/Ngoc_ke_long_ga_trung_non_chay_toi.png','','Suất',1,'còn hàng'),(19,'Oyakodon',75000,'assets/doan/Oyakodon.png','','Suất',1,'còn hàng'),(20,'Phở Chiên Phồng',75000,'assets/doan/Pho_Chien_Phong.png','','Suất',1,'còn hàng'),(21,'Phở Cuốn',75000,'assets/doan/Pho_Cuon.png','','Suất',1,'còn hàng'),(22,'Rump',75000,'assets/doan/Rump.png','','Suất',1,'còn hàng'),(23,'Sake Mentaiko Yaki',75000,'assets/doan/Sake_Mentaiko_Yaki.png','','Suất',1,'còn hàng'),(24,'Sake Roll',75000,'assets/doan/Sake_Roll.png','','Suất',1,'còn hàng'),(25,'Cold_Pressed_Juice_Antioxident',35000,'assets/douong/Cold_Pressed_Juice_Antioxident.png','','Cốc',2,'còn hàng'),(26,'Houjicha_Shiratama',45000,'assets/douong/Houjicha_Shiratama.png','','Cốc',2,'còn hàng'),(27,'Iced_Matcha_Latte',35000,'assets/douong/Iced_Matcha_Latte.png','','Cốc',2,'còn hàng'),(28,'Iced_Matcha_Shiratama',55000,'assets/douong/Iced_Matcha_Shiratama.png','','Cốc',2,'còn hàng'),(29,'Matcha_Float',45000,'assets/douong/Matcha_Float.png','','Cốc',2,'còn hàng'),(30,'Morico_Parfait',45000,'assets/douong/Morico_Parfait.png','','Cốc',2,'còn hàng');
/*!40000 ALTER TABLE `doan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donhang`
--

DROP TABLE IF EXISTS `donhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `donhang` (
  `madon` int(11) NOT NULL AUTO_INCREMENT,
  `makhach` int(11) DEFAULT NULL,
  `thoigiandat` bigint(20) NOT NULL,
  `thoigiannhan` bigint(20) NOT NULL,
  `diachinhan` varchar(250) DEFAULT NULL,
  `ghichu` varchar(250) DEFAULT NULL,
  `tongtien` decimal(18,0) DEFAULT NULL,
  `sdt` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `tenkhach` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`madon`),
  KEY `makhach` (`makhach`),
  CONSTRAINT `donhang_ibfk_1` FOREIGN KEY (`makhach`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donhang`
--

LOCK TABLES `donhang` WRITE;
/*!40000 ALTER TABLE `donhang` DISABLE KEYS */;
INSERT INTO `donhang` VALUES (1,4,1607363815703,0,'hà n?i','heeh',90000,'00909099875','abc'),(2,2,1607389108006,0,'haf nooij','bom hang',405000,'123','nam'),(3,2,1607413167816,0,'ha? nô?i','',210000,'123','nam');
/*!40000 ALTER TABLE `donhang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loaidoan`
--

DROP TABLE IF EXISTS `loaidoan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loaidoan` (
  `maloai` int(11) NOT NULL AUTO_INCREMENT,
  `tenloai` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`maloai`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loaidoan`
--

LOCK TABLES `loaidoan` WRITE;
/*!40000 ALTER TABLE `loaidoan` DISABLE KEYS */;
INSERT INTO `loaidoan` VALUES (1,'Đồ ăn'),(2,'Đồ uống'),(3,'Bánh kem');
/*!40000 ALTER TABLE `loaidoan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_USER'),(2,'ROLE_ADMIN'),(3,'ROLE_MODERATOR');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trangthai`
--

DROP TABLE IF EXISTS `trangthai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trangthai` (
  `matrangthai` int(11) NOT NULL AUTO_INCREMENT,
  `tentrangthai` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`matrangthai`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trangthai`
--

LOCK TABLES `trangthai` WRITE;
/*!40000 ALTER TABLE `trangthai` DISABLE KEYS */;
INSERT INTO `trangthai` VALUES (1,'Chờ xử lý'),(2,'Đã xác nhận'),(3,'Chuyển cửa hàng'),(4,'Còn hàng'),(5,'Hết hàng'),(6,'Đã xuất kho'),(7,'Hủy'),(8,'Chưa giao hàng'),(9,'Hoàn tất'),(10,'Đang giao');
/*!40000 ALTER TABLE `trangthai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trangthaidonhang`
--

DROP TABLE IF EXISTS `trangthaidonhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trangthaidonhang` (
  `matrangthai` int(11) NOT NULL,
  `madon` int(11) NOT NULL,
  `noidung` varchar(200) DEFAULT NULL,
  `ngaytao` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`matrangthai`,`madon`),
  KEY `madon` (`madon`),
  CONSTRAINT `trangthaidonhang_ibfk_1` FOREIGN KEY (`matrangthai`) REFERENCES `trangthai` (`matrangthai`),
  CONSTRAINT `trangthaidonhang_ibfk_2` FOREIGN KEY (`madon`) REFERENCES `donhang` (`madon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trangthaidonhang`
--

LOCK TABLES `trangthaidonhang` WRITE;
/*!40000 ALTER TABLE `trangthaidonhang` DISABLE KEYS */;
INSERT INTO `trangthaidonhang` VALUES (8,1,'',1607363815703),(8,2,'',1607389108006),(8,3,'',1607413167816);
/*!40000 ALTER TABLE `trangthaidonhang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `gt` varchar(5) DEFAULT 'nam',
  `sdt` varchar(50) DEFAULT NULL,
  `ngaytao` datetime DEFAULT CURRENT_TIMESTAMP,
  `trangthai` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'abc','123','mail@gmail.com','nam','09876543211','1999-05-19 00:00:00',_binary ''),(2,'nam123','$2a$10$pj6utYXrNuubdzRmsADPW.7sTvCYm6thgkzhv9Db76S5KNzd/FEp2','nam@gmail.com','nam',NULL,'2020-12-05 01:54:15',_binary ''),(3,'dung123','$2a$10$VhpvKgFdC14TkV6eM/Oceut4CV8OGf4KK0vH3.KQQfNEqmfqnZ.i6','dung123@gmail.com','nam',NULL,'2020-12-05 01:54:43',_binary ''),(4,'admin','$2a$10$VGHSmEBwob1dBkVXd2t/w.XPWWEjbt8Tvercbn/OPbpR7uxlI1LyC','ahihi@gmail.com','nam',NULL,'2020-12-06 04:39:29',_binary ''),(8,'user','$2a$10$4g2qtwGgw8D1qSI6UqhHnezNDvOo/PKV32G2aQcAUbb4I9EbvHbN.','user@gmail.com','nam',NULL,'2020-12-07 18:52:35',_binary ''),(9,'dwda','$2a$10$DyGDoUwceiwYY9PMZkZUYO/vfLHxHvEBI28PZxa2aV4/oTiiKlrlO','fsefsef@22gmail.com','nam',NULL,'2020-12-07 18:59:08',_binary '');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_roles` (
  `user_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`roles_id`),
  KEY `FKa62j07k5mhgifpp955h37ponj` (`roles_id`),
  CONSTRAINT `FK2o0jvgh89lemvvo17cbqvdxaa` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKa62j07k5mhgifpp955h37ponj` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (2,1),(3,1),(4,2),(8,2);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-12 17:39:59
